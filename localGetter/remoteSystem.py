import time
import pyautogui
from mylog import *
import keyboard
import mouse
from clientServer import *
from packetCreator import *
from collections import deque
import threading
class remoteSystem:
    def __init__(self, config):
        self.remoteAdd = config['address']
        self.remotePort = config['port']
        self.remoteName = config['name']
        self.protocolDict = getProtocolDict()
        self.wholeData = []        
        self.socketGuard = SocketGuard(self.parseData, self.remoteAdd, self.remotePort)
        

        self.commandList = deque([])
        self.startCommandPopper()

        

    def startCommandPopper(self):
        t = threading.Thread(target=self.commandPopper)
        t.setDaemon(True) #exit on main thread exit
        t.start()

    def pushCmdToQueue(self, cmd):
        logD("pushing cmd to queue", cmd)
        self.commandList.append(cmd.copy())
        
    def commandPopper(self):
        while(1):
            time.sleep(1) #TODO, remove this ;p
            if len(self.commandList) > 0:
                frame = self.commandList.popleft()
                logD('popd frame', frame)




    def getConnectionState(self): #TODO write more complicated connection state judgement system
        return self.socketGuard.isConnected()

    
    def steerMouseRelative(self, delX=0, delY=0, buttons=[0, 0, 0], scrolls=[0, 0]):
        packet = getMousePacket(delX, delY, buttons, scrolls)
#        logD('packets is my bussiness', packet)
        self.socketGuard.sendData(bytes(packet))
        

    def parseData(self, what):
        logI("info from socket", what.hex())

        wasPreviousEscape = False
        for i in range(len(what)):

            ##logging 
            appendData = ''
            if(what[i] in self.protocolDict.values() and not wasPreviousEscape):
                appendData = "  <<<< protocolValue "
                if(what[i] == self.protocolDict['escape']):
                    appendData = "  <<<< ESCAPE "
                    wasPreviousEscape = True
            else:
                wasPreviousEscape = False
            logD("bytes", "#{:02d}".format(i)+ " " + "{:02X}".format(what[i]) + appendData)
            ##logging 

            ##appending data to 'whole' buffer 

            self.wholeData.append(what[i])
            
        ##data analysis
        start = -1
        stop = -1

        wasLastEscape = False
        frames = []
        lastStop = 0

        for i in range(len(self.wholeData)):
            isSpecialSymbol = (self.wholeData[i] in self.protocolDict.values())
            if(isSpecialSymbol):
                if ((start == -1) and (self.wholeData[i] == self.protocolDict['start']) and (not wasLastEscape)):
                    start = i
                if ((start != -1) and (self.wholeData[i] == self.protocolDict['stop']) and (not wasLastEscape)):
                    stop = i
                
                if(stop!=-1 and start!=-1 and start < stop):
                    frame = self.wholeData[start+1:stop]
                    
                    bytesForFrame = []
                    lastEscape = False
                    for i in range(len(frame)):
                        if(frame[i] != self.protocolDict['escape'] or lastEscape):
                            bytesForFrame.append(frame[i])
                            lastEscape = False
                        else:
                            lastEscape = True

                    logD("frame len", len(frame))
                    logD("bytesForFrame len", len(bytesForFrame))
                    if(len(bytesForFrame) != len(frame)):
                        logV("LOOK HERE", "####################")
                    
                    for i in range(len(bytesForFrame)):
                        logD("bytes", "#{:02d} {:02X}".format(i, bytesForFrame[i]))
                    
                    frames.append(bytesForFrame.copy()) 
                    #self.wholeData[:stop+1] = [self.protocolDict['toDelete']]*(stop + 1)
                    if lastStop < stop:
                        lastStop = stop
                
                    start = -1
                    stop = -1


            wasLastEscape = (self.wholeData[i] == self.protocolDict['escape']) and (wasLastEscape==False)

        self.wholeData = [self.wholeData[i] for i in range(len(self.wholeData)) if i > lastStop]


        for frame in frames:
            logD("frames", frame)
            self.pushCmdToQueue(frame)











