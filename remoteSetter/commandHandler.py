import time
import pyautogui
from mylog import *
from serverside import *
from collections import deque
import threading
from mouse import mouseLogger


'''
def getFrameIdDict():
    FIDict = {
            "setMouse":    0x6D, #mouse steering incoming data
            "getMouse":    0x6E, #return mouse data to the socket that asked
        }
    return FIDict
'''


def getProtocolDict():
    prDict = {
            "start":    0xAB,
            "stop" :    0xBB,
            "packetId": 0xAE,
            "escape":   0xCD,
            "toDelete": 0xDE,
        }
    return prDict




class commandHandler():

    def __init__ (self):
        pyautogui.PAUSE = 0
        self.commandList = deque([])
        self.startCommandPopper()
        logI("cmdHandler", "inited")

        self.mouseLogger = mouseLogger()    

        self.prDict = getProtocolDict()
        
        self.commandsHandlers = {
            0x6D: self.moveMouseRel,
        }


    def pushCmdToQueue(self, cmd):
        logD("pushing cmd to queue", cmd)
        self.commandList.append(cmd.copy())


        #logD("full cmd list", self.commandList)


    def startCommandPopper(self):
        t = threading.Thread(target=self.commandPopper)
        t.setDaemon(True) #exit on main thread exit
        t.start()

    def commandPopper(self):
        while(1):
            #time.sleep(0.1) #TODO, remove this ;p
            if len(self.commandList) > 0:
                frame = self.commandList.popleft()
                logD('popd frame', bytes(frame))
                if(frame[0] == self.prDict["packetId"]):
                    if(frame[1] in self.commandsHandlers):
                        self.commandsHandlers[frame[1]](frame[2:])
            
            #logD("len in commandPopper", len(self.commandList))


    def moveMouseRel(self, data):
        logV("len of data", len(data))
        relativeX = int.from_bytes(data[0:4], "big") - 2**30
        relativeY = int.from_bytes(data[4:8], "big") - 2**30
        leftButton = data[8]
        middleButton = data[9]
        rightButton = data[10]
        scrollUpDown = data[11] - 2**7
        scrollRightLeft = data[12] - 2**7
        

        logD("analyzed", (relativeX, relativeY, leftButton, middleButton, rightButton, scrollUpDown, scrollRightLeft))
    
        self.mouseLogger.passData([relativeX, relativeY, leftButton, middleButton, rightButton, scrollUpDown, scrollRightLeft])



if __name__ == "__main__":
    a = commandHandler()
    command = [174, 109, 64, 0, 0, 11, 63, 255, 255, 253, 0, 0, 0, 128, 129]
    #a.pushCmdToQueue(command)
    #command = [174, 109, 64, 0, 0, 11, 63, 255, 255, 253, 0, 0, 0, 128, 128]
    a.pushCmdToQueue(command)

    time.sleep(1)






