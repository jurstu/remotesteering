import socket
import time
from threading import Thread
import os
from mylog import *

def getSpaces(much):
    if(type(much) == int):
        return "            "*(much+2)
    else:
        return ""



class SocketGuard(object):
    
    def __init__(self, dataCb, address, port):
        self.add = address
        self.port = port # 0-1024 require permissions on X
        self.dataCb = dataCb
        self.end = False
        self.connected = False
        self.start()

    def start(self):
        self.thread = Thread(target=self.run, args=())
        self.thread.daemon = True  # Daemonize thread
        self.thread.start()  # Start the execution

    def isConnected(self):
        return self.connected

    def run(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.s.setblocking(0)
        logI("client", "awaiting for server on other side...")
        while not self.end:
            self.connected = False

            try:
                self.s.connect((self.add, self.port))        # blocking command
            except Exception as e:
#                print(e)
#                self.s.close()
#                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                time.sleep(1)
                continue

            logI("client", "connected")

            self.connected = True
            try:
                while not self.end:
                    data = self.s.recv(200)              # blocking command

                    self.dataCb(data)
                    if(len(data) == 0):
                        self.connected = False
                        logW("client", "disconnected")

                        self.s.close()
                        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        time.sleep(0.1)
                        logI("client", "awaiting for server on other side...")
                        break

                    time.sleep(0.1)
            except Exception as e:
                self.connected = False
                logE("client", str(e))
                logW("client", "disconnected")

                self.s.close()
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                time.sleep(0.1)
                logI("client", "awaiting for server on other side...")

    
    def sendData(self, what):
        '''requires bytes'''

        if(what == ""):
            return
        try:
            if self.connected:
                self.s.send(what)
            else:
                logW("client", "client id DC-d")
        except Exception as e:
            logI("client", e)
            #TODO






def printData(data):
    logI("client asdf", str(data))

if __name__ == "__main__":
    a = SocketGuard(printData, "127.0.0.1", 1025)
    
    a.start()
    c = 0
    d = []
    while(1):
        time.sleep(0.5)
        c+=1
        d.append(c%256)
        try:
            a.sendData(bytes(d))
        except Exception as e:
            logI("client", e)
            exit()









