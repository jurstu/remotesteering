import time
import pyautogui
from mylog import *
import keyboard
import mouse
from clientServer import *



def getFrameIdDict():
    FIDict = {
            "setMouse":    0x6D, #mouse steering incoming data
            "getMouse":    0x6E, #return mouse data to the socket that asked
            "getMonitors": 0x6F, #return monitors data to the socket that asked 
            
        }
    return FIDict

def getProtocolDict():
    prDict = {
            "start":    0xAB,
            "stop" :    0xBB,
            "packetId": 0xAE,
            "escape":   0xCD,
            "toDelete": 0xDE,
        }
    return prDict

def getEscaped(packet):
    protocolDict = getProtocolDict()
    frameIdDict = getFrameIdDict()
    escaped = []
    for byte in packet:
        if(not byte in protocolDict.values()):
            escaped.append(byte)
        else:
            escaped.append(protocolDict["escape"])
            escaped.append(byte)

    return escaped


def getMousePacket(delX=0, delY=0, buttons=[0, 0, 0], scrolls=[0, 0]):
        byteArray = []
        protocolDict = getProtocolDict()
        frameIdDict = getFrameIdDict()
        #
        #    4 bytes delta X || 4 bytes delta Y || 3 buttons bytes || 2 scrolls bytes 
        #

        delX += 2**30
        temp = delX.to_bytes(4, 'big') #good enough for +-2**29 inputs
        for byte in temp:
            byteArray.append(byte)
            
        delY += 2**30
        temp = delY.to_bytes(4, 'big') #good enough for +-2**29 inputs
        for byte in temp:
            byteArray.append(byte)



        for point in buttons:
            try:
                temp = bytes([point])
                for byte in temp:
                    byteArray.append(byte)
            except Exception as e:
                logE("serverHandler", str(e))
                byteArray.append(0)

            
        for point in scrolls:
            point += 2**7
            try:
                temp = bytes([point])
                for byte in temp:
                    byteArray.append(byte)
            except Exception as e:
                logE("serverHandler", str(e))
                byteArray.append(0)


        escaped = getEscaped(byteArray)

        sendArray = []
        sendArray.append(protocolDict['start'])
        sendArray.append(protocolDict['packetId'])
        packetId = frameIdDict['setMouse']
        if(not packetId in protocolDict.values()):
            sendArray.append(packetId)
        else:
            sendArray.append(protocolDict["escape"])
            sendArray.append(packetId)

        for byte in escaped:
            sendArray.append(byte)
        sendArray.append(protocolDict['stop'])

        return sendArray
        
        









if __name__ == "__main__":
    a = wholeCollector()
    while(1):
        time.sleep(1)
