import threading

# pip3 install pynput
from mylog import *
from pynput.mouse import Listener

import pyautogui
import pymouse
import time
mouse = pymouse.PyMouse()

#TODO add dynamic screen size for RPI or any other local computer
class mouseGrabber():
    def __init__(self, dataCb, correctMouse):
        self.dataCb = dataCb
        self.correctMouse = correctMouse
        pyautogui.FAILSAFE = False
        self.startMouseThread()
        self.lastBtns = [0,0,0]

    def startMouseThread(self):
        t = threading.Thread(target=self.mouseWatcher)
        t.setDaemon(True)
        t.start()

        d = threading.Thread(target=self.mouseCorrector)
        d.setDaemon(True)
        d.start()

    def on_click(self, x, y, button, pressed):
        val = 0
        if(pressed):
            val = 1
        else:
            val = 2

        if button.name == "left":
            ind = 0

        if button.name == "middle":
            ind = 1

        if button.name == "right":
            ind = 2


        btns = [0,0,0]
        self.lastBtns = btns
        btns[ind] = val
        if self.correctMouse():
            self.dataCb(buttons = btns)
        

    def on_scroll(self, x, y, dx, dy):
        if self.correctMouse():
            self.dataCb(scrolls=[dx, dy], buttons = self.lastBtns)

    def mouseWatcher(self):
        with Listener(on_click=self.on_click, on_scroll=self.on_scroll) as listener:
            listener.join()

    def mouseCorrector(self):
        global mouse
        while(1):
            pos = mouse.position()
            centerX = 1920//2
            centerY = 1080//2       #TODO!! ! ! ! ! ! 
            if(pos[0] != centerX or pos[1] != centerY):
                try:
                    if self.correctMouse():
                        pyautogui.moveTo(centerX, centerY)
                        deltaX = pos[0] - centerX
                        deltaY = pos[1] - centerY
                        self.dataCb(delX = deltaX, delY = deltaY, buttons = self.lastBtns)
                except Exception as e:
                    logE("mouseCorrector error", str(e)[:100])

                #print(deltaX, deltaY)
            #time.sleep(0.01)


#dataCallback function proto
def printsmth(delX=0, delY=0, buttons=[0, 0, 0], scrolls=[0, 0]):
    logD("testing", "delX: {:04d}, delY: {:04d}, buttons: {}, scrolls: {}".format(delX, delY, buttons, scrolls))

    
