import time
import pyautogui
from mylog import *
import keyboard
import mouse
from clientServer import *
from packetCreator import *
from remoteSystem import *
import json



class wholeCollector():

    def __init__ (self):
        
        config = self.loadConfig()

        self.remoteSystem = remoteSystem(config['remotes'][0])

        self.isConnectionMade = self.remoteSystem.getConnectionState
        self.mouseGrabber = mouse.mouseGrabber(self.remoteSystem.steerMouseRelative, self.isConnectionMade)

        logI("wholeCollector", "inited")

    def printDatas(self, what):
        logI("info from socket", what)
        
    def loadConfig(self):
        f = open('config.json', 'r')
        data = f.read()
        data = json.loads(data)
    
        return data


    





if __name__ == "__main__":
    a = wholeCollector()
    while(1):
        time.sleep(1)
