import socket
import time
from threading import Thread
from mylog import *

class SocketGuard:
    def __init__(self, dataCb, address, port):
        self.add = address
        self.port = port
        self.dataCb = dataCb

        self.undigested = ""
        self.end = False
        self.connected = False
        self.start()

    def start(self):
        self.thread = Thread(target=self.run, args=())
        self.thread.daemon = True  # Daemonize thread to kill upon main code escape
        self.thread.start()  # Start the execution

    def run(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1) #do we need to reuse port ? 
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        self.s.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 1)
        self.s.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 1)
        self.s.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT, 5) 

        self.s.bind((self.add, self.port))
        self.s.listen(0)

        while not self.end:
            self.connected = False
            
            self.c, self.address = self.s.accept()
            self.connected = True
            logD("socketGuard", "connected")
            
            while not self.end:
                data = ''
                try:
                    data = self.c.recv(1024)
                except TimeoutError:
                    #print("###########timeout##############")    
                    break
                except Exception as e:
                    logE("socketGuard", str(e))
                    self.c.close()
                    break
                self.dataCb(data)

                if (len(data) == 0):
                    break
                
            self.c.close()
            self.connected = False
            logW("socketGuard", "DCD")

    def sendData(self, what):
        if(what == ""):
            return -1
        try:
            if self.connected:
                #print(getSpaces(self.number) + str(what) + "--->")
                self.c.send(what)
                return 0
            else:
                #print("dc at %d-th socket"%self.number)
                return -2
        except Exception as e:
            logE("socketGuard", str(e))
            #TODO

        return -3


def printsmth(data):
    logD("testing", str(data))


if __name__ == "__main__":
    a = SocketGuard(printsmth, "127.0.0.1", 1025)
    dt = 0.2
    c = 200
    while(1):
        c+=1
        c=c%256
        a.sendData(bytes([c]))
        
        time.sleep(dt)



