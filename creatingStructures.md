# TODO

remote setter:
    -moving mouse / keyboard
    -reporting screen data (size, number of monitors)
    -reporting mouse / (keyboard?)
    -gui for generating configs




RPI code: (local getter)
    -class: relay station:
        -has config:
            when to switch to which remote computer with steering:
                (monitor-wise  /  keyboards-shortcuts  / smth-else)     # TODO
            remoteSteeredComputers:
                ipAddresses / configs   # TODO
            inputs collector config     # TODO

        -class: inputs collector # TODO
            -has config -> is it a headless system / full X raspberry # TODO
            -returns to relay station any mouse/keyboard movement to act  # TODO keyboard
            -headless input gatherer # TODO

        -class: remoteSteeredComputer -> many of those
            -socket To Specific Address from config # TODO
            -connection Health - ping + didItDCwithErrors # TODO
            -remote Data: mouse xy, ? ? ?               # TODO
            -data parser + handler -> status data       # semi-done


all config text-based ? 






# TODO

1. class - remote steered computer ->
    has its config
    has its connection state
    embeds a socketClientGuard
    has a parser + handler
    has x,y of mouse / handles event from socket 
    has methods like 'move mouse offset' or 'press a key' to pass from relay station
    handles problems with communication -> returns a state of connection


2. config manager for relay station
    json format for addresses / configs
    loading
    checking if complete, 
    autocomplete if incomplete
    3 configs for : relay station, remoteComputers, inputs collector 





