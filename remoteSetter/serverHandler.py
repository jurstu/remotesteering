import time
import pyautogui
from mylog import *
from serverside import *
from commandHandler import *






class wholeCollector():

    def __init__ (self):
        logI("wholeCollector", "server inited")

        self.cmdHandler = commandHandler()
        self.protocolDict = getProtocolDict()
        self.wholeData = []
        self.serverGuard = SocketGuard(self.parseData, "192.168.0.153", 1025)


    def parseData(self, what):
        # tricky line to test parser
        #what = b'\xab\xae\x6d\x3f\xff\xcd\xae\xf8\x40\x00\x00\x00\x00\x00\x00\x80\x80\xbb\xff\xab\xae\x6d\x3f\xff\xff\xf8\x40\x00\x00\x00\x00\x00\x00\x80\x80\xbb\xfe\xab\xff\xbb'
        logI("info from socket", what.hex())

        wasPreviousEscape = False
        for i in range(len(what)):

            ##logging 
            appendData = ''
            if(what[i] in self.protocolDict.values() and not wasPreviousEscape):
                appendData = "  <<<< protocolValue "
                if(what[i] == self.protocolDict['escape']):
                    appendData = "  <<<< ESCAPE "
                    wasPreviousEscape = True
            else:
                wasPreviousEscape = False
            logD("bytes", "#{:02d}".format(i)+ " " + "{:02X}".format(what[i]) + appendData)
            ##logging 

            ##appending data to 'whole' buffer 

            self.wholeData.append(what[i])
            
        ##data analysis
        start = -1
        stop = -1

        wasLastEscape = False
        frames = []
        lastStop = 0

        for i in range(len(self.wholeData)):
            isSpecialSymbol = (self.wholeData[i] in self.protocolDict.values())
            if(isSpecialSymbol):
                if ((start == -1) and (self.wholeData[i] == self.protocolDict['start']) and (not wasLastEscape)):
                    start = i
                if ((start != -1) and (self.wholeData[i] == self.protocolDict['stop']) and (not wasLastEscape)):
                    stop = i
                
                if(stop!=-1 and start!=-1 and start < stop):
                    frame = self.wholeData[start+1:stop]
                    
                    bytesForFrame = []
                    lastEscape = False
                    for i in range(len(frame)):

                        if(frame[i] != self.protocolDict['escape'] or lastEscape):
                            bytesForFrame.append(frame[i])
                            lastEscape = False
                        else:
                            lastEscape = True

                    logD("frame len", len(frame))
                    logD("bytesForFrame len", len(bytesForFrame))
                    if(len(bytesForFrame) != len(frame)):
                        logV("LOOK HERE", "####################")
                    
                    for i in range(len(bytesForFrame)):
                        logD("bytes", "#{:02d} {:02X}".format(i, bytesForFrame[i]))
                    
                    frames.append(bytesForFrame.copy()) 
                    #self.wholeData[:stop+1] = [self.protocolDict['toDelete']]*(stop + 1)
                    if lastStop < stop:
                        lastStop = stop
                
                    start = -1
                    stop = -1


            wasLastEscape = (self.wholeData[i] == self.protocolDict['escape']) and (wasLastEscape==False)

        logD("lastStop", lastStop)
        self.wholeData = [self.wholeData[i] for i in range(len(self.wholeData)) if i > lastStop]

        logD("data", self.wholeData)
        for frame in frames:
            self.cmdHandler.pushCmdToQueue(frame)


        what = b'\xab\xae\x6d\x3f\xff\xcd\xae\xf8\x40\x00\x00\x00\x00\x00\x00\x80\x80\xbb\xff\xab\xae\x6d\x3f\xff\xff\xf8\x40\x00\x00\x00\x00\x00\x00\x80\x80\xbb\xfe\xab\xff\xbb'
        self.serverGuard.sendData(what)




if __name__ == "__main__":
    setLogLevel(ERROR)
    a = wholeCollector()

    
    while(1):
        time.sleep(1)
