import time

lin = 1
if lin:
    import sys


NOLOG = 10
ERROR = 20
WARNING = 30
INFO = 40
DEBUG = 50
VERBOSE = 60


__levelStrings = ["-", "-", "E", "W", "I", "D", "V"]
__addTime = False
__ignoredTopics = {}
__level = VERBOSE

def logE(topic, data):
    '''logs as a Error level'''
    __log(data, topic, ERROR)
        
def logW(topic, data):
    '''logs as a Warining level'''
    __log(data, topic, WARNING)
        
def logI(topic, data):
    '''logs as a Info level'''
    __log(data, topic, INFO)
        
def logD(topic, data):
    '''logs as a Debug level'''
    __log(data, topic, DEBUG)
        
def logV(topic, data):
    '''logs as a Verbose level'''
    __log(data, topic, VERBOSE)
        

def logIgnore(topic):
    ''' disables topic from logging'''      
    global __ignoredTopics
    __ignoredTopics[str(topic)] = 'y'

def logRmIgnore(topic):
    ''' enables logging topic '''
    global __ignoredTopics
    if str(topic) in __ignoredTopics:
        del __ignoredTopics[str(topic)]

def setLogLevel(l):
    ''' sets log level, see mylog.ERROR / WARNING ... '''
    global __level
    __level = l

def setAddTime(doAddTime):
    ''' switches time logging depending on argument ''' 
    global __addTime
    __addTime = bool(doAddTime)

def __log(data, topic=-1, level=DEBUG):
    
    global __level
    global __levelStrings
    global __addTime
    global __ignoredTopics
    topic = str(topic)
    if(__level >= level and not topic in __ignoredTopics):
        ls = ""
        ti = ""
        if level//10 < len(__levelStrings):
            ls = __levelStrings[level//10] + ":"

        if __addTime:
            ti = str(time.time())

        if (not lin):
            print(ls, ti, topic + ":", data)
        else:
            data = str(data)
            topic = str(topic)
            ti = str(ti)
            if(level == VERBOSE):
                sys.stdout.write('\033[95m' + ls + ' ' + ti + topic + ": " + data + '\x1b[0m' + '\n')

            if(level == DEBUG):
                sys.stdout.write('\033[94m' + ls + ' ' + ti + topic + ": " + data + '\x1b[0m' + '\n')

            if(level == INFO):
                sys.stdout.write('\033[92m' + ls + ' ' + ti + topic + ": " + data + '\x1b[0m' + '\n')

            if(level == WARNING):
                sys.stderr.write('\x1b[1;33m' + ls + ' ' + ti + topic + ": " + data + '\x1b[0m' + '\n')

            if(level == ERROR):
                sys.stderr.write('\x1b[1;31m' + ls + ' ' + ti + topic + ": " + data + '\x1b[0m' + '\n')

            if(level == NOLOG):
                sys.stdout.write('\x1b[1;37m' + ls + ' ' + ti + topic + ": " + data + '\x1b[0m' + '\n')



if __name__ == "__main__":
        
        #EXAMPLE OF USE

        setLogLevel(VERBOSE)
        logV("hello", "Verbose")
        logD("hello", "debug")
        logI("hello", "info")
        logW("hello", "warn")
        logE("hello", "error")


        logIgnore("hello")
        logRmIgnore("hello")
        #setLogLevel(ERROR)
#        logD("hello", "asdf")





