from mylog import *
import pyautogui

class mouseLogger:
    def __init__(self):
        self.leftButton = 0
        self.middleButton = 0
        self.rightButton = 0
        pyautogui.FAILSAFE = False


    def passData(self, data):
        logD("in mouse.py, Data", data)
        

        pyautogui.moveRel(data[0], data[1])


        if(self.leftButton != data[2]):
            self.leftButton = data[2]
            if(self.leftButton):
                pyautogui.mouseDown(button='left')
            else:
                pyautogui.mouseUp(button='left')


        if(self.middleButton != data[3]):
            self.middleButton = data[3]
            if(self.middleButton):
                pyautogui.mouseDown(button='middle')
            else:
                pyautogui.mouseUp(button='middle')


        if(self.rightButton != data[4]):
            self.rightButton = data[4]
            if(self.rightButton):
                pyautogui.mouseDown(button='right')
            else:
                pyautogui.mouseUp(button='right')



        pyautogui.hscroll(data[5])
        pyautogui.scroll(data[6])


