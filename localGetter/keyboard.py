


import threading
from pynput.keyboard import Key, Listener
import pynput.keyboard as keyboard


def startKeyboardThread():
    t = threading.Thread(target=keyboardWatcher)
    t.setDaemon(True)
    t.start()

'''
def on_press(key):
    print(key)
    print(type(key))
    if(isinstance(key, keyboard._xorg.KeyCode)):  
        try:      
            print("\t\t\t\tnormal key: " + str(key.char))
        except Exception as e:
            print(e)


    if(not isinstance(key, keyboard._xorg.KeyCode)): 
        print("\t\t\t\tspecial key: " + key.name)
'''


def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))

def on_release(key):
    print(key)
    #pass to bigger code TODO
    if(isinstance(key, keyboard._xorg.KeyCode)):        
        print("normal key: " + str(key.char))

    if(not isinstance(key, keyboard._xorg.KeyCode)): 
        print("special key: " + key.name)




def keyboardWatcher():
    with Listener(on_press=on_press, on_release=on_release) as listener:
        listener.join()


