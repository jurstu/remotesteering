# this is a sneaky clone of *synergy* product

the idea is to create a python3 code to listen for keyboard / mouse inputs
and allow to pass it through to any computer using tcp sockets. First idea
is to run it on raspberry (or any SBC) connected to your local network
and be able to switch between controlled computers using keyboard macros.

Then -> on the other computer there is a tcp server listening for sockets
trying to controll it, 

some hashing/crypting algorythms may be added down the road.





# installs:
pip3 install pyautogui

pip3 install pymouse

pip3 install pynput

pip3 install tabulate


